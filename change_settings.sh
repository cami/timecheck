#!/bin/sh
# This script is released under the GPLv3 license
# See COPYING file for more information

LCL_PATH=$(pwd -P)

checkConfig(){
	if [ -f "$LCL_PATH/timecheck.conf" ]
		then
			. "$LCL_PATH/timecheck.conf"
		else
			echo "Config file not found. Do you run this script in the right directory? Script execution aborted." 
			exit 1
	fi
}

isWhiptailInstalled() {
	command -v whiptail > /dev/null 2>&1
	EXITCODE="$?"
	if [ "$EXITCODE" = 0 ]
		then
			USE_WHIPTAIL=true
		else
			USE_WHIPTAIL=false
	fi
}

getUser(){
	if [ "$USE_WHIPTAIL" = "true" ]
		then
			USER=$(whiptail --inputbox "Which user should be controlled by Timecheck?" 8 60 "$USER" --title "Timecheck konfiguration" 3>&1 1>&2 2>&3)
			EXITCODE="$?"
			if [ "$EXITCODE" = "0" ]
				then
					if [ -z "$USER" ]
						then
							echo "Invalid username! Script execution aborted."
							exit 1
					fi
				else
					echo "An error occured! Script execution aborted."
					exit 1
			fi

		else
			printf "Which user should be controlled by Timecheck? [$USER] "
			read -r USER_NEW
			if [ ! -z "$USER_NEW" ]
				then
					USER="$USER_NEW"
			fi
	fi
}

getTime(){
	if [ "$USE_WHIPTAIL" = "true" ]
		then
			TIME_MOFR=$(whiptail --inputbox "How many minutes should $USER be allowed to use the account from monday to friday?" 9 60 "$TIME_MOFR" --title "Timecheck configuration" 3>&1 1>&2 2>&3)
			EXITCODE="$?"
			if [ ! "$EXITCODE" = "0" ]
				then
					echo "Invalid input! Script execution aborted."
					exit 1
			fi
			TIME_SASU=$(whiptail --inputbox "How many minutes should $USER be allowed to use the account at the weekend?" 9 60 "$TIME_SASU" --title "Timecheck configuration" 3>&1 1>&2 2>&3)
			EXITCODE="$?"
			if [ ! "$EXITCODE" = "0" ]
				then
					echo "Invalid input! Script execution aborted."
					exit 1
			fi
		else
			printf "How many minutes should $USER be allowed to use the account from monday to friday? [$TIME_MOFR] "
			read -r TIME_MOFR_NEW
			if [ ! -z "$TIME_MOFR_NEW" ]
				then
					TIME_MOFR=TIME_MOFR_NEW
			fi
			printf "How many minutes should $USER be allowed to use the account at the weekend? [$TIME_SASU] "
			read -r TIME_SASU_NEW
			if [ ! -z "$TIME_MOFR_NEW" ]
				then
					TIME_SASU=TIME_SASU_NEW
			fi
	fi
}

checkPermissions(){
	INSTALL_USER=$(whoami)
	if [ ! "$INSTALL_USER" = "root" ]
		then
			echo "You don't have enough permissions to perform this script. Please retry as root. Script execution aborted."
			exit 1
	fi
}

allCorrect(){
	if [ "$USE_WHIPTAIL" = "true" ]
		then
			whiptail --yesno "Are these information correct? \n \n User to be controlled: $USER \n Time from monday to friday: $TIME_MOFR \n Time from Saturday to Sunday: $TIME_SASU \n" 13 60 --defaultno --title "Timecheck configuration" 3>&1 1>&2 2>&3
			EXITCODE="$?"
			if [ ! "$EXITCODE" = "0" ]
				then
					echo "Please correct the information by running this script again! Script execution aborted."
					exit 1
			fi
		else
			echo "We have collected some information. Please have a look at it"
			echo "User to be controlled:		$USER"
			echo "Time from monday to friday:	$TIME_MOFR"
			echo "Time at the weekend:		$TIME_SASU"
			echo ""
			printf "Are these information correct? [y|N] "
			read -r INFROMATION_CORRECT
			if [ -z $INFORMATION_CORRECT ]
				then
					echo "Please correct the information by running this script again. Script execution aborted."
					exit 1
				else
					case $INFORMATION_CORRECT in
						y|Y)
						;;
						n|N) echo "Please correct the information by running this script again. Script execution aborted." && exit 1
						;;
						*) echo "Something went wrong. Script execution aborted." && exit 1
						;;
					esac
			fi
	fi
}

exportVariable(){
	sed -i 's/USER=.*/USER='"$USER"'/' "$LCL_PATH"/"timecheck.conf"
	sed -i 's/TIME_MOFR=.*/TIME_MOFR='"$TIME_MOFR"'/' "$LCL_PATH"/"timecheck.conf"
	sed -i 's/TIME_SASU=.*/TIME_SASU='"$TIME_SASU"'/' "$LCL_PATH"/"timecheck.conf"
}


main() {
	isWhiptailInstalled
	checkConfig
	checkPermissions
	getUser
	getTime
	allCorrect
	exportVariable
}
main
